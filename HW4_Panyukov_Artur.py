from functools import reduce


# problem 9
res_9 = [a * b * (1000 - a - b) for a in range(1, 999)
         for b in range(a, 999) if a**2 + b**2 == (1000 - a - b)**2][0]
print(res_9)


# problem 6
res_6 = sum(range(101))**2 - sum([x**2 for x in range(101)])
print(res_6)


# problem 48
res_48 = reduce(lambda x, y: (x + y) % 10**10, [x**x for x in range(1, 1001)])
print(res_48)


# problem 40
string = ''.join(map(str, range(1, 190000)))
res_40 = reduce(lambda x, y: x*y, [int(string[z-1])
                                   for z in [10**a for a in range(7)]])
print(res_40)

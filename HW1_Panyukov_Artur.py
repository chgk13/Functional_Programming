from functools import reduce


def add(x, y):
    return x + y


def less(x):
    return x < 1000000


sum_1 = reduce(add, filter(less, map(lambda x: x**2, range(1000))))
sum_1_1 = reduce(add, filter(less, map(lambda x: x**2, range(1000000))))
print(sum_1, sum_1 == sum_1_1)

sum_2 = sum([x**2 for x in range(1, 1000) if x**2 < 1000000])
sum_2_2 = sum([x**2 for x in range(1, 1000000) if x**2 < 1000000])
print(sum_2, sum_2 == sum_2_2)

sum_3 = 0
for x in range(1000):
    if x**2 > 1000000:
        break
    sum_3 += x**2
sum_3_3 = 0
for x in range(1000000):
    if x**2 >= 1000000:
        break
    sum_3_3 += x**2
print(sum_3, sum_3 == sum_3_3)

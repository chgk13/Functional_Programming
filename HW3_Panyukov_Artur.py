from functools import reduce


def collatz_steps(n):
    assert isinstance(n, int) and n > 0, 'n must be a natural number'

    def next(n):
        return 3*n + 1 if n % 2 else n//2

    def col(n):
        res = [1] if n == 1 else [l.append(next(l[-1]))
                                  or l for l in [[n]] for x in range(n)
                                  if l[-1] != 1][0]
        return len(res) - 1 if res[-1] == 1 else len(res) - 1 + col(res[-1])

    return col(n)


def test():
    assert collatz_steps(16) == 4
    assert collatz_steps(12) == 9
    assert collatz_steps(1000000) == 152


test()
